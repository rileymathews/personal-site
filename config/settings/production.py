from .base import *

DEBUG = False
SECRET_KEY = get_env_variable("SECRET_KEY")
ALLOWED_HOSTS = ["rileymathews.com"]
CSRF_COOKIE_DOMAIN = "rileymathews.com"
CSRF_TRUSTED_ORIGINS = ["https://rileymathews.com"]
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db/db.sqlite3"),
    }
}

try:
    from .local import *
except ImportError:
    pass
