from django import template
from wagtail.models import Page

register = template.Library()

@register.inclusion_tag("navbar.html")
def navbar():
    return {
        "menu_pages": Page.objects.in_menu()
    }
